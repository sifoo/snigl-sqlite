#include <assert.h>

#include "snigl/pool.h"

#include "snigl_sqlite/db.h"
#include "snigl_sqlite/lib.h"

struct sqlite_db *sqlite_db_open(struct sqlite_lib *lib, const char *path) {
  struct sqlite_db *db = sgl_malloc(&lib->db_pool);
  db->nrefs = 1;
  return db;
}

void sqlite_db_deref(struct sqlite_db *db, struct sqlite_lib *lib) {
  assert(db->nrefs > 0);
  
  if (!--db->nrefs) {
    sgl_free(&lib->db_pool, db);
  }
}

