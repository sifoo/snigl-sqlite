#ifndef SNIGL_SQLITE_DB_H
#define SNIGL_SQLITE_DB_H

#include "snigl/int.h"
#include "snigl/ls.h"

struct sqlite_lib;

struct sqlite_db {
  struct sgl_ls ls;
  sgl_int_t nrefs;
};

struct sqlite_db *sqlite_db_open(struct sqlite_lib *lib,
                                 const char *path);

void sqlite_db_deref(struct sqlite_db *db, struct sqlite_lib *lib);

#endif
