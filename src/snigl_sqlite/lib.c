#include <stdio.h>
#include <stdlib.h>

#include "snigl/sgl.h"
#include "snigl/str.h"

#include "snigl_sqlite/db.h"
#include "snigl_sqlite/lib.h"
#include "snigl_sqlite/types/db.h"

static bool open_db_imp(struct sgl_fimp *fimp, struct sgl *sgl, struct sgl_op **rpc) {
  struct sgl_val *p = sgl_pop(sgl);
  struct sqlite_lib *sl = sgl_baseof(struct sqlite_lib, lib, fimp->func->def.lib);
  sgl_push(sgl, sl->DB)->as_ptr = sqlite_db_open(sl, sgl_str_cs(p->as_str));
  sgl_val_free(p, sgl);
  return true;
}

static bool lib_init(struct sgl_lib *l, struct sgl *sgl, struct sgl_pos *pos) {
  struct sqlite_lib *sl = sgl_baseof(struct sqlite_lib, lib, l);
  
  sl->DB = sqlite_db_type_new(sgl, pos, l, sgl_sym(sgl, "DB"),
                              sgl_types(sgl->T));

  sgl_lib_add_cfunc(l, sgl, pos, 
                    sgl_sym(sgl, "open-db"),
                    open_db_imp,
                    sgl_rets(sl->DB),
                    sgl_arg(sgl->Str)); 

  return true;
}

static void lib_deinit(struct sgl_lib *l, struct sgl *sgl) {
  struct sqlite_lib *sl = sgl_baseof(struct sqlite_lib, lib, l);
  sgl_pool_deinit(&sl->db_pool);
  free(sl);
}

struct sgl_lib *sqlite_lib_new(struct sgl *sgl, struct sgl_pos *pos) {
  struct sqlite_lib *sl = malloc(sizeof(struct sqlite_lib));

  sgl_pool_init(&sl->db_pool,
                sizeof(struct sqlite_db),
                offsetof(struct sqlite_db, ls));

  struct sgl_lib *l = &sl->lib;
  sgl_lib_init(l, sgl, pos, sgl_sym(sgl, "sqlite"));
  l->init = lib_init;
  l->deinit = lib_deinit;
  return l;
}

bool snigl_sqlite_init(struct sgl *sgl, struct sgl_pos *pos) {
  return sgl_add_lib(sgl, pos, sqlite_lib_new(sgl, pos));
}
