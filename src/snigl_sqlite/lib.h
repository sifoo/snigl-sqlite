#ifndef SNIGL_SQLITE_LIB_H
#define SNIGL_SQLITE_LIB_H

#include "snigl/lib.h"
#include "snigl/pool.h"

struct sqlite_lib {
  struct sgl_lib lib;
  struct sgl_pool db_pool;
  struct sgl_type *DB;
};

struct sgl_lib *sqlite_new(struct sgl *sgl, struct sgl_pos *pos);
bool snigl_sqlite_init(struct sgl *sgl, struct sgl_pos *pos);

#endif
