#include "snigl/buf.h"
#include "snigl/error.h"
#include "snigl/lib.h"
#include "snigl/sgl.h"
#include "snigl/type.h"
#include "snigl/val.h"

#include "snigl_sqlite/db.h"
#include "snigl_sqlite/lib.h"
#include "snigl_sqlite/types/db.h"

static enum sgl_cmp cmp_val(struct sgl_val *val, struct sgl_val *rhs) {
  return sgl_ptr_cmp(val->as_ptr, rhs->as_ptr);
}

static void deinit_val(struct sgl_val *val, struct sgl *sgl) {
  struct sqlite_lib *sl = sgl_baseof(struct sqlite_lib, lib, val->type->def.lib);
  sqlite_db_deref(val->as_ptr, sl);
}

static void dump_val(struct sgl_val *val, struct sgl_buf *out) {
  sgl_buf_printf(out, "(DB %p)", val->as_ptr);
}

static void dup_val(struct sgl_val *val, struct sgl *sgl, struct sgl_val *out) {
  struct sqlite_db *db = out->as_ptr = val->as_ptr;
  db->nrefs++;
}

static bool is_val(struct sgl_val *val, struct sgl_val *rhs) {
  return val->as_ptr == rhs->as_ptr;
}

struct sgl_type *sqlite_db_type_new(struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_lib *lib,
                                    struct sgl_sym *id,
                                    struct sgl_type *parents[]) {
  struct sgl_type *t = sgl_type_new(sgl, pos, lib, id, parents);
  t->cmp_val = cmp_val;
  t->deinit_val = deinit_val;
  t->dump_val = dump_val;
  t->dup_val = dup_val;
  t->is_val = is_val;
  return t;
}
