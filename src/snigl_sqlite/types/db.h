#ifndef SNIGL_SQLITE_TYPES_DB_H
#define SNIGL_SQLITE_TYPES_DB_H


struct sgl_lib;
struct sgl_sym;
struct sgl_type;

struct sgl_type *sqlite_db_type_new(struct sgl *sgl,
                                    struct sgl_pos *pos,
                                    struct sgl_lib *lib,
                                    struct sgl_sym *id,
                                    struct sgl_type *parents[]);

#endif
